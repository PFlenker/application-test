create table application_test(
	id  INT UNSIGNED AUTO_INCREMENT,
	cover_url VARCHAR(255),
	title VARCHAR(255),
	year VARCHAR(30),
	pages INT UNSIGNED,
	description TEXT,
	PRIMARY KEY (id)
);

LOAD DATA INFILE '/tmp/Bookfinder-Sampledata.csv' INTO TABLE tbl_name
  FIELDS TERMINATED BY ';' ENCLOSED BY '"'
  LINES TERMINATED BY '\n'
  IGNORE 1 LINES;

